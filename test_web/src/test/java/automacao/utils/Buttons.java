package automacao.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Buttons {
	
	static WebElement button;
	
	public static void addCostumerButton(WebDriver driver) throws InterruptedException {
		Thread.sleep(2000);
		button = driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a"));
		button.click();
		Thread.sleep(5000);
		System.out.println("Concluindo passo 3");
	}
	
	public static void saveButton(WebDriver driver) throws InterruptedException {
		Thread.sleep(2000);
		button = driver.findElement(By.xpath("//*[@id=\"form-button-save\"]"));
		button.click();
		Thread.sleep(5000);
	}

}
