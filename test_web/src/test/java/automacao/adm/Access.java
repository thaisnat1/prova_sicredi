package automacao.adm;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import automacao.utils.Buttons;

public class Access {
	
	static Buttons button = new Buttons();
	
	public static void selecionaCategoria(WebDriver driver) {
		WebElement campo_select = driver.findElement(By.xpath("//*[@id=\"switch-version-select\"]"));
		campo_select.click();
		campo_select = driver.findElement(By.xpath("/html/body/div[1]/select/option[2]"));
		campo_select.click();
		System.out.println("Concluindo Passo 2");
	}
	
	public static void preencheFormulario(WebDriver driver) throws InterruptedException {
		
		WebElement campo_name, campo_lastname, campo_contactfirstname, campo_phone, campo_addressline1,
		 campo_addressline2, campo_city, campo_state, campo_postalcode, campo_country , campo_creditlimit, campo_fromemployeer;
		
		//Formulario form = new Formulario();
		
		campo_name = driver.findElement(By.xpath("//*[@id=\"field-customerName\"]"));
		campo_name.sendKeys("Teste Sicredi");
		
		campo_lastname = driver.findElement(By.xpath("//*[@id=\"field-contactLastName\"]"));
		campo_lastname.sendKeys("Teste");
		
		campo_contactfirstname = driver.findElement(By.xpath("//*[@id=\"field-contactFirstName\"]"));
		campo_contactfirstname.sendKeys("Thais Nicoly");
		
		campo_phone = driver.findElement(By.xpath("//*[@id=\"field-phone\"]"));
		campo_phone.sendKeys("51 9999-9999");
		
		campo_addressline1 = driver.findElement(By.xpath("//*[@id=\"field-addressLine1\"]"));
		campo_addressline1.sendKeys("Av Assis Brasil, 3970");
		
		campo_addressline2 = driver.findElement(By.xpath("//*[@id=\"field-addressLine2\"]"));
		campo_addressline2.sendKeys("Torre D");
		
		campo_city = driver.findElement(By.xpath("//*[@id=\"field-city\"]"));
		campo_city.sendKeys("Porto Alegre");
		
		campo_state = driver.findElement(By.xpath("//*[@id=\"field-state\"]"));
		campo_state.sendKeys("RS");
		
		campo_postalcode = driver.findElement(By.xpath("//*[@id=\"field-postalCode\"]"));
		campo_postalcode.sendKeys("91000-000");
		
		campo_country = driver.findElement(By.xpath("//*[@id=\"field-country\"]"));
		campo_country.sendKeys("Brasil");
		
		campo_fromemployeer = driver.findElement(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/a"));
		campo_fromemployeer.click();
		
		campo_fromemployeer = driver.findElement(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/div/ul/li[8]"));
		campo_fromemployeer.click(); 
		
		campo_creditlimit = driver.findElement(By.xpath("//*[@id=\"field-creditLimit\"]"));
		campo_creditlimit.sendKeys("200");
		
		System.out.println("Concluindo passo 4 ");
		Thread.sleep(2000);
		Buttons.saveButton(driver);
		System.out.println("Concluindo passo 5");
	}
	
	public static void validaMensagem(WebDriver driver) {
		
		String mensagem = driver.findElement(By.xpath("//*[@id=\"report-success\"]/p")).getText();
		assertEquals("Your data has been successfully stored into the database. Edit Customer or Go back to list" , mensagem);
		System.out.println("Concluindo passo 6");
	}

}
