package automacao.adm;

import java.util.Scanner;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import automacao.utils.Buttons;

public class Menu {
	
	static WebDriver driver;
	static Scanner sc = new Scanner(System.in);
	static Access access = new Access();
	static Buttons button = new Buttons();

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");
		java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.SEVERE);
		driver = new ChromeDriver();
		System.out.println(" \nIniciando Desafio !! - Passo 1");
		
		driver.get("https://www.grocerycrud.com/demo/bootstrap_theme");
		
		Access.selecionaCategoria(driver);
		Buttons.addCostumerButton(driver);
		Access.preencheFormulario(driver);
		Access.validaMensagem(driver);
		driver.close();
		System.out.println("Concluindo passo 7 - Fim do desafio!!");

	}
	
}
